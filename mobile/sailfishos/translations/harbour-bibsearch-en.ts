<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en">
<context>
    <name></name>
    <message id="bibsearch-application-title">
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source>BibSearch</source>
        <translation>BibSearch</translation>
    </message>
    <message id="bibsearch-application-about">
        <location filename="../qml/pages/AboutPage.qml" line="45"/>
        <source>About BibSearch</source>
        <translation>About BibSearch</translation>
    </message>
    <message id="bibsearch-version">
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message id="bibsearch-copyright-line">
        <location filename="../qml/pages/AboutPage.qml" line="65"/>
        <source>\u00a9 2016\u20132019 Thomas Fischer</source>
        <translation>© 2016–2019 Thomas Fischer</translation>
    </message>
    <message id="bibsearch-under-gpl">
        <location filename="../qml/pages/AboutPage.qml" line="79"/>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</source>
        <translation>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</translation>
    </message>
    <message id="bibsearch-link-label-project-hompage">
        <location filename="../qml/pages/AboutPage.qml" line="89"/>
        <source>Project Homepage</source>
        <translation>Project Homepage</translation>
    </message>
    <message id="bibsearch-based-on-kbibtex">
        <location filename="../qml/pages/AboutPage.qml" line="97"/>
        <source>This program is sharing its code base with KBibTeX, the bibliography editor using KDE technology.</source>
        <translation>This program is sharing its code base with KBibTeX, the bibliography editor using KDE technology.</translation>
    </message>
    <message id="bibsearch-link-label-kbibtex-homepage">
        <location filename="../qml/pages/AboutPage.qml" line="107"/>
        <source>KBibTeX&apos;s Homepage</source>
        <translation>KBibTeX&apos;s Homepage</translation>
    </message>
    <message id="bibsearch-link-label-git-repository">
        <location filename="../qml/pages/AboutPage.qml" line="116"/>
        <source>Git Repository</source>
        <translation>Git Repository</translation>
    </message>
    <message id="bibsearch-link-label-report-issue">
        <location filename="../qml/pages/AboutPage.qml" line="134"/>
        <source>Report Issue</source>
        <translation>Report Issue</translation>
    </message>
    <message id="resultlist-waiting-for-results">
        <location filename="../qml/pages/BibliographyListView.qml" line="40"/>
        <source>Waiting for results \u2026</source>
        <translation>Waiting for results …</translation>
    </message>
    <message id="resultlist-pulldown-new-search">
        <location filename="../qml/pages/BibliographyListView.qml" line="42"/>
        <source>Pull down to start a new search.</source>
        <translation>Pull down to start a new search.</translation>
    </message>
    <message id="pulldownmenu-about">
        <location filename="../qml/pages/BibliographyListView.qml" line="107"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message id="pulldownmenu-settings">
        <location filename="../qml/pages/BibliographyListView.qml" line="112"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message id="pulldownmenu-new-search">
        <location filename="../qml/pages/BibliographyListView.qml" line="117"/>
        <source>New Search</source>
        <translation>New Search</translation>
    </message>
    <message id="label-author">
        <location filename="../qml/pages/EntryView.qml" line="48"/>
        <location filename="../qml/pages/SearchForm.qml" line="95"/>
        <source>Author</source>
        <translation>Author</translation>
    </message>
    <message id="label-title">
        <location filename="../qml/pages/EntryView.qml" line="63"/>
        <location filename="../qml/pages/SearchForm.qml" line="82"/>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message id="label-publication">
        <location filename="../qml/pages/EntryView.qml" line="78"/>
        <source>Publication</source>
        <translation>Publication</translation>
    </message>
    <message id="label-year">
        <location filename="../qml/pages/EntryView.qml" line="93"/>
        <source>Year</source>
        <translation>Year</translation>
    </message>
    <message id="label-doi">
        <location filename="../qml/pages/EntryView.qml" line="109"/>
        <source>DOI</source>
        <translation>DOI</translation>
    </message>
    <message id="label-url">
        <location filename="../qml/pages/EntryView.qml" line="111"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message id="label-found-via">
        <location filename="../qml/pages/EntryView.qml" line="127"/>
        <source>Found via</source>
        <translation>Found via</translation>
    </message>
    <message id="pulldownmenu-view-online">
        <location filename="../qml/pages/EntryView.qml" line="146"/>
        <source>View Online</source>
        <translation>View Online</translation>
    </message>
    <message id="searchengines-title">
        <location filename="../qml/pages/SearchEngineListView.qml" line="37"/>
        <source>Available Search Engines</source>
        <translation>Available Search Engines</translation>
    </message>
    <message id="searchparameters-title">
        <location filename="../qml/pages/SearchForm.qml" line="44"/>
        <source>Search Parameters</source>
        <translation>Search Parameters</translation>
    </message>
    <message id="label-search-engines">
        <location filename="../qml/pages/SearchForm.qml" line="49"/>
        <location filename="../qml/pages/SettingsPage.qml" line="63"/>
        <source>Search Engines</source>
        <translation>Search Engines</translation>
    </message>
    <message id="selected-count-none">
        <location filename="../qml/pages/SearchForm.qml" line="52"/>
        <location filename="../qml/pages/SettingsPage.qml" line="66"/>
        <source>None selected</source>
        <translation>None selected</translation>
    </message>
    <message id="selected-count-numarg">
        <location filename="../qml/pages/SearchForm.qml" line="54"/>
        <location filename="../qml/pages/SettingsPage.qml" line="68"/>
        <source>%1 selected</source>
        <translation>%1 selected</translation>
    </message>
    <message id="label-selected-atleastone">
        <location filename="../qml/pages/SearchForm.qml" line="57"/>
        <location filename="../qml/pages/SettingsPage.qml" line="71"/>
        <source>At least one search engine must be selected.</source>
        <translation>At least one search engine must be selected.</translation>
    </message>
    <message id="label-free-text">
        <location filename="../qml/pages/SearchForm.qml" line="68"/>
        <source>Free Text</source>
        <translation>Free text</translation>
    </message>
    <message id="pulldownmenu-start-searching">
        <location filename="../qml/pages/SearchForm.qml" line="110"/>
        <source>Start Searching</source>
        <translation>Start Searching</translation>
    </message>
    <message id="settings-title">
        <location filename="../qml/pages/SettingsPage.qml" line="40"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message id="label-sort-order">
        <location filename="../qml/pages/SettingsPage.qml" line="46"/>
        <source>Sort Order</source>
        <translation>Sort Order</translation>
    </message>
    <message id="sortorder-humanreadable-lastname-newestfirst">
        <location filename="../src/bibliographymodel.cpp" line="92"/>
        <source>Last name, newest first</source>
        <translation>Last name, newest first</translation>
    </message>
    <message id="sortorder-humanreadable-lastname-oldestfirst">
        <location filename="../src/bibliographymodel.cpp" line="94"/>
        <source>Last name, oldest first</source>
        <translation>Last name, oldest first</translation>
    </message>
    <message id="sortorder-humanreadable-newestfirst-lastname">
        <location filename="../src/bibliographymodel.cpp" line="96"/>
        <source>Newest first, last name</source>
        <translation>Newest first, last name</translation>
    </message>
    <message id="sortorder-humanreadable-oldestfirst-lastname">
        <location filename="../src/bibliographymodel.cpp" line="98"/>
        <source>Oldest first, last name</source>
        <translation>Oldest first, last name</translation>
    </message>
    <message id="wherepublished-doctoral-dissertation">
        <location filename="../src/bibliographymodel.cpp" line="278"/>
        <source>Doctoral dissertation</source>
        <translation>Doctoral dissertation</translation>
    </message>
    <message id="wherepublished-doctoral-dissertation-arg">
        <location filename="../src/bibliographymodel.cpp" line="281"/>
        <source>Doctoral dissertation (%1)</source>
        <translation>Doctoral dissertation (%1)</translation>
    </message>
    <message id="shortauthors-two-author-args">
        <location filename="../src/bibliographymodel.cpp" line="296"/>
        <source>%1 and %2</source>
        <translation>%1 and %2</translation>
    </message>
    <message id="shortauthors-one-author-arg-and-n-others-arg">
        <location filename="../src/bibliographymodel.cpp" line="299"/>
        <source>%1 and %2 more</source>
        <translation>%1 and %2 more</translation>
    </message>
    <message id="human-readable-two-search-engines">
        <location filename="../src/searchenginelist.cpp" line="171"/>
        <source>%1 and %2</source>
        <extracomment>Two search engines selected</extracomment>
        <translation>%1 and %2</translation>
    </message>
    <message id="human-readable-three-search-engines">
        <location filename="../src/searchenginelist.cpp" line="175"/>
        <source>%1, %2, and %3</source>
        <extracomment>Three search engines selected</extracomment>
        <translation>%1, %2, and %3</translation>
    </message>
    <message id="human-readable-four-search-engines">
        <location filename="../src/searchenginelist.cpp" line="179"/>
        <source>%1, %2, %3, and %4</source>
        <extracomment>Four search engines selected</extracomment>
        <translation>%1, %2, %3, and %4</translation>
    </message>
    <message id="human-readable-five-search-engines">
        <location filename="../src/searchenginelist.cpp" line="183"/>
        <source>%1, %2, %3, %4, and %5</source>
        <extracomment>Five search engines selected</extracomment>
        <translation>%1, %2, %3, %4, and %5</translation>
    </message>
    <message id="human-readable-six-search-engines">
        <location filename="../src/searchenginelist.cpp" line="187"/>
        <source>%1, %2, %3, %4, %5, and %6</source>
        <extracomment>Six search engines selected</extracomment>
        <translation>%1, %2, %3, %4, %5, and %6</translation>
    </message>
    <message id="human-readable-seven-search-engines">
        <location filename="../src/searchenginelist.cpp" line="191"/>
        <source>%1, %2, %3, %4, %5, %6, and %7</source>
        <extracomment>Seven search engines selected</extracomment>
        <translation>%1, %2, %3, %4, %5, %6, and %7</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message id="onlinesearch-acmdigitallibrary-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchacmportal.cpp" line="115"/>
        <source>ACM Digital Library</source>
        <translation>ACM Digital Library</translation>
    </message>
    <message id="onlinesearch-arxiv-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearcharxiv.cpp" line="675"/>
        <source>arXiv.org</source>
        <translation>arXiv.org</translation>
    </message>
    <message id="onlinesearch-bibsonomy-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchbibsonomy.cpp" line="230"/>
        <source>Bibsonomy</source>
        <translation>Bibsonomy</translation>
    </message>
    <message id="onlinesearch-googlescholar-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchgooglescholar.cpp" line="462"/>
        <source>Google Scholar</source>
        <translation>Google Scholar</translation>
    </message>
    <message id="onlinesearch-ieeexplore-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchieeexplore.cpp" line="187"/>
        <source>IEEEXplore</source>
        <translation>IEEEXplore</translation>
    </message>
    <message id="onlinesearch-ingentaconnect-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchingentaconnect.cpp" line="364"/>
        <source>IngentaConnect</source>
        <translation>IngentaConnect</translation>
    </message>
    <message id="onlinesearch-jstor-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchjstor.cpp" line="120"/>
        <source>JSTOR</source>
        <translation>JSTOR</translation>
    </message>
    <message id="onlinesearch-pubmed-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchpubmed.cpp" line="149"/>
        <source>PubMed</source>
        <translation>PubMed</translation>
    </message>
    <message id="onlinesearch-sciencedirect-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchsciencedirect.cpp" line="218"/>
        <source>ScienceDirect</source>
        <translation>ScienceDirect</translation>
    </message>
    <message id="onlinesearch-springerlink-label">
        <location filename="../../../src/networking/onlinesearch/onlinesearchspringerlink.cpp" line="299"/>
        <source>SpringerLink</source>
        <translation>SpringerLink</translation>
    </message>
</context>
</TS>
